import QtQuick 2.0
import Sailfish.Silica 1.0

Dialog {
    id: intervalSelector
    // help property to overwrite only when User set the Interval
    property int setInterval: 0
    canAccept: !(intervalPicker.value <= 10)
    onAccepted: {
        // overwrite the global Interval
        myInterval = setInterval
        // set startButton status
        startButtonstatus = true
        // show selected interval in the label
        timerLabelvalue = qsTr("Your interval is %1 secounds").arg(myInterval)
    }

    // Column to draw the items
    Column {
        spacing: Theme.paddingLarge
        anchors.fill: parent
        DialogHeader {
            acceptText: qsTr("Select")
        }

        // Label with input information
        Label {
            wrapMode: Text.WordWrap
            text: qsTr("Select an interval for your workout, which fits you best.")
            anchors {
                left: parent.left
                right: parent.right
                margins: Theme.paddingLarge
            }
        }

        // set your workout interval time
        Slider {
            id: intervalPicker
            width: parent.width
            minimumValue: 10
            maximumValue: maxInterval
            stepSize: 10
            label: qsTr("Your workout intervall is %1 secounds").arg(intervalPicker.value)

            onValueChanged: {
                // set page Interval
                setInterval = intervalPicker.value
                console.log(intervalPicker.value)
            }
        }
    }
}

