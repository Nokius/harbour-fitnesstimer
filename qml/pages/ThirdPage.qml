import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: settings

    /**
      TODO:
      User can configure beepinterval
   **/

    // Column to draw the items
    Column {
        spacing: Theme.paddingLarge
        width: parent.width
        // Settings
        PageHeader {
            id: settingsHeader
            title: qsTr("Settings")
        }
        // Custom peparation time
        SectionHeader {
            text: qsTr("Customisation")
        }
        // enable custom peparation time
        TextSwitch {
            id: enableCustomisationTextSwitch
            checked: enableCustomisation
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Enable")

            onVisibleChanged: {
                enableCustomisation = enableCustomisationTextSwitch.checked
            }
        }
        Column {
            spacing:  Theme.paddingLarge
            width: parent.width
            visible: enableCustomisationTextSwitch.checked

            // Select the custom set-up time
            Slider {
                id:setupTimePicker
                width: parent.width
                value: setupTime
                minimumValue: 5
                maximumValue: 60
                stepSize: 5
                label: qsTr("Your set-up time in %1 secounds").arg(setupTimePicker.value)

                onValueChanged: {
                    setupTime = setupTimePicker.value
                    console.log(setupTimePicker.value)
                }
            }

            // Select maxInterval
            Slider {
                id:maxIntervalPicker
                width: parent.width
                value: maxInterval
                minimumValue: 60
                maximumValue: 3600
                stepSize: 60
                label: qsTr("Your maximum interval in %1 secounds").arg(maxIntervalPicker.value)

                onValueChanged: {
                    maxInterval = maxIntervalPicker.value
                    console.log(maxIntervalPicker.value)
                }
            }
        }
    }
}
