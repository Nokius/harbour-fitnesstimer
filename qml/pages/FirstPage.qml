import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.6

Page {
    id: fitnesstimer
    property int getreadyTime: setupTime
    property int countdownSeconds: myInterval

    // The effective value will be restricted by ApplicationWindow.allowedOrientations
    allowedOrientations: Orientation.All

    // To enable PullDownMenu, place our content in a SilicaFlickable
    SilicaFlickable {
        anchors.fill: parent

        // PullDownMenu and PushUpMenu must be declared in SilicaFlickable, SilicaListView or SilicaGridView
        PullDownMenu {
            MenuItem {
                text: qsTr("Settings")
                onClicked: pageStack.push(Qt.resolvedUrl("ThirdPage.qml"))
            }

            MenuItem {
                text: qsTr("Set Interval")
                onClicked: pageStack.push(Qt.resolvedUrl("SecondPage.qml"))
            }
        }

        // Column to draw the items
        Column {
            spacing: Theme.paddingLarge
            anchors.fill: parent
            // Fitnesstimer
            PageHeader {
                id: timerHeader
                title: qsTr("Fitnesstimer")
            }

            // ProgressCircle show the process
            ProgressCircle {
                id: timerCircle
                //visible: innerTimer.running
                height: 400
                width: 400
                anchors.horizontalCenter: parent.horizontalCenter
                //progressColor: Theme.highlightColor
                //backgroundColor: Theme.highlightDimmerColor

                Timer {
                    repeat: true
                    interval: 1000
                    onTriggered: timerCircle.value = (timerCircle.value + 0.1) % 1.0
                    running: intervalTimer.running
                }
            }

            // Label to show the remaning time
            Label {
                id: timerLabel
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: Theme.fontSizeExtraLarge
                color: Theme.highlightColor
                text: timerLabelvalue
            }

            // IconButton to start the timer
            IconButton {
                id: startButton
                anchors.horizontalCenter: parent.horizontalCenter
                enabled: startButtonstatus
                icon.source: "image://theme/icon-l-play"
                onClicked: {
                    getreadyTimer.start();
                    startButtonstatus = false
                }
            }

            // Timer to get ready for the workout
            Timer {
                id: getreadyTimer
                repeat: true
                interval: 1000
                onTriggered: {
                    timerLabelvalue = qsTr("Get ready in %1 secounds").arg(fitnesstimer.getreadyTime)
                    fitnesstimer.getreadyTime--;
                    if (fitnesstimer.getreadyTime == 3) {
                        console.log("get ready")
                        playStart.play()
                    }
                    if (fitnesstimer.getreadyTime == 0) {
                        console.log("preTimer finished")
                        timerLabelvalue = qsTr("GO!!")
                        intervalTimer.start(fitnesstimer.countdownSeconds = myInterval)
                        running = false;
                        fitnesstimer.getreadyTime = setupTime
                    }
                }
            }

            // Timer to countdown thx to eetu for this \o/
            Timer {
                id: intervalTimer
                repeat: true
                interval: 1000
                onTriggered: {
                    fitnesstimer.countdownSeconds--;
                    timerLabelvalue = qsTr("You have %1 secounds left").arg(fitnesstimer.countdownSeconds)
                    if (fitnesstimer.countdownSeconds > 10 && fitnesstimer.countdownSeconds % 10 == 0) {
                        console.log(countdownSeconds)
                        playBeep.play()
                    }
                    if (fitnesstimer.countdownSeconds == 10) {
                        console.log("last ten sec")
                        playBeeploop.play()
                    }
                    if (fitnesstimer.countdownSeconds == 0) {
                        running = false;
                        timerLabelvalue = qsTr("Your interval is %1 secounds").arg(myInterval)
                        playFinish.play()
                        fitnesstimer.countdownSeconds = myInterval
                        startButtonstatus = true
                    }
                }
            }

            // Audio played when getreadyTimer is at the last three second
            Audio {
                id: playStart
                source: "../../sounds/start.wav"
            }

            // Audio played every ten secound a beep
            Audio {
                id: playBeep
                source: "../../sounds/beep.wav"
            }

            // Audio countdown for the last ten secounds
            Audio {
                id: playBeeploop
                source: "../../sounds/beep.wav"
                loops : 13
            }
            // Audio played when workout is done
            Audio {
                id:playFinish
                source: "../../sounds/finish.wav"
            }
        }
    }
}
