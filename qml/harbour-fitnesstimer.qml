import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow
{
    // sets the workout interval
    property int myInterval: 0
    // allow set-up time
    property int setupTime: 10
    // User set max interval timer
    property int maxInterval: 300
    // Label string
    property string timerLabelvalue: qsTr("Select your workout interval")
    // status of customisation switch
    property bool enableCustomisation: false
    // start button behavior
    property bool startButtonstatus: false


    initialPage: Component { FirstPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations
}
